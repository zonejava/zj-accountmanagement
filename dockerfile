FROM openjdk:11.0.7-slim-buster
MAINTAINER Nanoo (arnaudlaval33@gmail.com)
ADD target/account-management.jar account-management.jar
ENTRYPOINT ["java","-jar","account-management.jar"]