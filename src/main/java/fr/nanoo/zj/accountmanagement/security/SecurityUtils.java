package fr.nanoo.zj.accountmanagement.security;

import lombok.experimental.UtilityClass;
import lombok.extern.slf4j.Slf4j;

import javax.servlet.http.HttpServletRequest;

/**
 * @author nanoo - created : 08/07/2020 - 11:59
 */
@Slf4j
@UtilityClass
public class SecurityUtils {

    /**
     * get the token in http request
     * @param request Http request
     * @return token
     */
    public String getToken(HttpServletRequest request){
        String token = request.getHeader("Authorization");
        return token != null ? token.replace("Bearer ","") : "";
    }

}
