package fr.nanoo.zj.accountmanagement.utils.keycloak;

import fr.nanoo.zj.accountmanagement.application.errors.FunctionalException;
import fr.nanoo.zj.accountmanagement.model.dtos.UserAccountDto;
import lombok.extern.slf4j.Slf4j;
import org.keycloak.OAuth2Constants;
import org.keycloak.admin.client.Keycloak;
import org.keycloak.admin.client.KeycloakBuilder;
import org.keycloak.admin.client.resource.RealmResource;
import org.keycloak.admin.client.resource.UserResource;
import org.keycloak.admin.client.resource.UsersResource;
import org.keycloak.representations.idm.CredentialRepresentation;
import org.keycloak.representations.idm.RoleRepresentation;
import org.keycloak.representations.idm.UserRepresentation;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import java.net.URI;
import java.util.Collections;

/**
 * @author nanoo - created : 26/05/2020 - 17:57
 */
@Service
@Slf4j
public class KeycloakUtils {

    @Value("${keycloak.auth-server-url}")
    private String keycloakBaseUrl;
    @Value("${keycloak.realm}")
    public String realm;
    @Value("${keycloak.username}")
    public String username;
    @Value("${keycloak.password}")
    public String password;
    @Value("${keycloak.resource}")
    private String clientId;
    @Value("${keycloak.secret}")
    public String clientSecret;

    /**
     * Create a new User in Keycloak Authentication server.
     * @param newUser dto containing data to store in Keycloak user
     * @return userId from keycloak auth server
     */
    public String createUser(UserAccountDto newUser) {

        // Get Resources
        Keycloak keycloak = getInstance();
        RealmResource realmResource = keycloak.realm(realm);
        UsersResource usersResource = realmResource.users();

        // Create User Representation with userAccountDto
        UserRepresentation user = getUserRepresentation(newUser);

        // Create user (requires manage-users role)
        Response response = usersResource.create(user);
        String userId = getCreatedId(response);
        response.close();

        // Get user
        UserResource userResource = usersResource.get(userId);

        // Get the default role "user"
        RoleRepresentation userRealmRole = realmResource.roles().get("user").toRepresentation();

        // Assign realm role "user" to user
        userResource
                .roles()
                .realmLevel()
                .add(Collections.singletonList(userRealmRole));

        return userId;

    }

    /**
     * Remove user from Keycloak Authentication server
     * @param id user id to remove from auth server
     */
    public void deleteUser (String id){
        // Get Resources
        Keycloak keycloak = getInstance();
        RealmResource realmResource = keycloak.realm(realm);
        UsersResource usersResource = realmResource.users();

        // Delete User
        try {
            usersResource.get(id).remove();
        }catch (Exception e){
            throw new FunctionalException("Error during removing user in keycloak database : " + e.getMessage());
        }
    }

//    /**
//     * check mail availability in keycloak DB.
//     * @return true when available
//     */
//    public boolean checkMailOrUsername (String mail){
//        // Get Resources
//        Keycloak keycloak = getInstance();
//        RealmResource realmResource = keycloak.realm(realm);
//
//        UsersResource usersResource = realmResource.users();
//        int users = usersResource.count(null,null,mail,null);
//        log.debug(String.valueOf(users));
//        return users == 0;
//    }
//
//    /**
//     * check username availability in keycloak DB.
//     * @return true when available
//     */
//    public boolean checkUsername (String username){
//        // Get Resources
//        Keycloak keycloak = getInstance();
//        RealmResource realmResource = keycloak.realm(realm);
//        UsersResource usersResource = realmResource.users();
//        List<UserRepresentation> users = usersResource.search(username);
//        log.debug(String.valueOf(users.size()));
//        if (!users.isEmpty()) {
//            for (UserRepresentation user : users) {
//                if (user.getUsername().equalsIgnoreCase(username))
//                    return false;
//            }
//        }
//        return true;
//    }

    /**
     * Create Keycloak instance with parameters contains in configuration file.
     * @return Keycloak instance
     */
    private Keycloak getInstance (){
        return KeycloakBuilder.builder()
                .serverUrl(keycloakBaseUrl)
                .realm(realm)
                .grantType(OAuth2Constants.PASSWORD)
                .clientId(clientId)
                .clientSecret(clientSecret)
                .username(username)
                .password(password)
                .build();
    }

    /**
     * Create UserRepresentation with data from userAccountDto.
     * @param newUser user containing data
     * @return userRepresentation
     */
    private UserRepresentation getUserRepresentation(UserAccountDto newUser) {

        UserRepresentation user = new UserRepresentation();
        user.setEnabled(true);
        user.setUsername(newUser.getUsername());
        user.setEmail(newUser.getEmail());

        CredentialRepresentation credentialRepresentation = new CredentialRepresentation();
        credentialRepresentation.setTemporary(false);
        credentialRepresentation.setType(CredentialRepresentation.PASSWORD);
        credentialRepresentation.setValue(newUser.getPassword());
        user.setCredentials(Collections.singletonList(credentialRepresentation));

        return user;
    }

    /**
     * Get id from the response receive when you call auth server to create user.
     * @param response response of auth server
     * @return id if exist in response or throws a WebApplicationException
     */
    private String getCreatedId(Response response) {
        URI location = response.getLocation();
        if (!response.getStatusInfo().equals(Response.Status.CREATED)) {
            Response.StatusType statusInfo = response.getStatusInfo();
            throw new WebApplicationException("Create method returned status " +
                    statusInfo.getReasonPhrase() + " (Code: " + statusInfo.getStatusCode() + "); expected status: Created (201)", response);
        }
        if (location == null) {
            return null;
        }
        String path = location.getPath();
        return path.substring(path.lastIndexOf('/') + 1);
    }

}
