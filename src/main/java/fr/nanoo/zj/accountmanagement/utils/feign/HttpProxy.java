package fr.nanoo.zj.accountmanagement.utils.feign;

import fr.nanoo.zj.accountmanagement.model.dtos.UserAccountDto;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

/**
 * @author nanoo - created : 11/06/2020 - 11:55
 */
@FeignClient(name = "gateway", url = "${GATEWAY_URL:http://localhost:9000}")
public interface HttpProxy {

    /* Save in tutorial service */
    @PutMapping("/${TUTORIAL_APP_NAME:TUTORIAL-DEV}/account/save")
    UserAccountDto saveUserInTutorialManagementDb(@RequestHeader("Authorization") String accessToken,
                                                  @RequestBody UserAccountDto accountDto);

    /* Delete in tutorial service */
    @DeleteMapping("/${TUTORIAL_APP_NAME:TUTORIAL-DEV}/account/remove/{id}")
    void removeUserInTutorialManagementDb(@RequestHeader("Authorization") String accessToken,
                                          @PathVariable("id") String id);

    /* Save in commentary service */
    @PutMapping("/${COMMENTARY_APP_NAME:COMMENTARY-DEV}/account/save")
    UserAccountDto saveUserInCommentaryManagementDb(@RequestHeader("Authorization") String accessToken,
                                                    @RequestBody UserAccountDto accountDto);

    /* Delete in commentary service */
    @DeleteMapping("/${COMMENTARY_APP_NAME:COMMENTARY-DEV}/account/remove/{id}")
    void removeUserInCommentaryManagementDb(@RequestHeader("Authorization") String accessToken,
                                            @PathVariable("id") String id);
}
