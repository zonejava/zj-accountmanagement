package fr.nanoo.zj.accountmanagement.service.contract;

import fr.nanoo.zj.accountmanagement.model.dtos.UserAccountDto;

import java.util.List;

/**
 * @author nanoo - created : 26/05/2020 - 14:54
 */

public interface UserAccountService {

    /**
     * This method create user in Keycloak auth server and if success,
     * persist user account in DB with keycloak user id as externalId.
     * - first in Account management service
     * - seconde in Tutorial management service
     * - and finally in Commentary management service (not yet implemented)
     * @param accessToken token to propagate
     * @param accountDto user account to save
     * @return a user account if it is saved in DB
     */
    UserAccountDto createAccount(String accessToken, UserAccountDto accountDto);

    /**
     * This method return the list of all user account in DB.
     * @return a list of user account
     */
    List<UserAccountDto> getUserAccountList();

    /**
     * This method search a user account in db with it external id.
     * @param externalId the external id of user account
     * @return a user account if exist
     */
    UserAccountDto getAccountByExternalId (String externalId);

    /**
     * This method delete a user account from both kaycloak auth server and
     * all services DB.
     * @param accessToken token to propagate
     * @param externalId external id of user to remove
     */
    void deleteUserAccount(String accessToken, String externalId);

    /**
     * this method call keycloak api to check if mail is available
     * @param mail mail we want to check
     * @return true if mail available
     */
    boolean isMailAvailable(String mail);

    /**
     * this method call keycloak api to check if username is available
     * @param username username we want to check
     * @return true if mail available
     */
    boolean isUsernameAvailable(String username);
}
