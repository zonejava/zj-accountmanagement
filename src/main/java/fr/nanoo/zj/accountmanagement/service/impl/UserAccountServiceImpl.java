package fr.nanoo.zj.accountmanagement.service.impl;

import fr.nanoo.zj.accountmanagement.application.errors.FunctionalException;
import fr.nanoo.zj.accountmanagement.db.UserAccountRepository;
import fr.nanoo.zj.accountmanagement.model.dtos.UserAccountDto;
import fr.nanoo.zj.accountmanagement.model.mappers.UserAccountMapper;
import fr.nanoo.zj.accountmanagement.service.contract.UserAccountService;
import fr.nanoo.zj.accountmanagement.utils.feign.HttpProxy;
import fr.nanoo.zj.accountmanagement.utils.keycloak.KeycloakUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import javax.ws.rs.WebApplicationException;
import java.time.LocalDateTime;
import java.util.List;

/**
 * @author nanoo - created : 26/05/2020 - 15:08
 */
@Service
@Slf4j
@Transactional
public class UserAccountServiceImpl implements UserAccountService {

    private final UserAccountRepository userAccountRepository;
    private final UserAccountMapper userAccountMapper;

    private final KeycloakUtils keycloakUtils;
    private final HttpProxy httpProxy;

    @Autowired
    public UserAccountServiceImpl(UserAccountRepository userAccountRepository,
                                  UserAccountMapper userAccountMapper,
                                  KeycloakUtils keycloakUtils,
                                  HttpProxy httpProxy) {
        this.userAccountRepository = userAccountRepository;
        this.userAccountMapper = userAccountMapper;
        this.keycloakUtils = keycloakUtils;
        this.httpProxy = httpProxy;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public UserAccountDto createAccount(String accessToken, UserAccountDto accountDto) {

        /* First register on Keycloak authentication server */
        String keycloakId = persistInKeycloakAuthServer(accountDto);
        /* Second persist userAccount in DB */
        accountDto = persistInAccountManagementDb(accountDto, keycloakId);
        /* Third persist user in other services DB */
        persistInTutorialManagementDb(accessToken, accountDto);
        persistInCommentaryManagementDb(accessToken, accountDto);

        return accountDto;
   }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<UserAccountDto> getUserAccountList() {
        return userAccountMapper.fromEntityListToDtoList(
                userAccountRepository.findAll()
        );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public UserAccountDto getAccountByExternalId(String externalId) {
        return userAccountMapper.fromEntityToDto(
                userAccountRepository.findByExternalId(externalId).orElseThrow()
        );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void deleteUserAccount(String accessToken, String externalId){
        /* First remove from DB */
        try {
            userAccountRepository.deleteByExternalId(externalId);
            httpProxy.removeUserInTutorialManagementDb(accessToken, externalId);
            httpProxy.removeUserInCommentaryManagementDb(accessToken, externalId);
        }catch (Exception e){
            log.error("Error during deletion of user in DB : " + e.getMessage());
            throw new FunctionalException("Error during deletion of user in DB : " + e.getMessage());
        }

        /* Second remove from server auth Keycloak */
        keycloakUtils.deleteUser(externalId);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isMailAvailable(String mail) {
        return userAccountRepository.findByEmail(mail).orElse(null) == null;
    }

    /**
     *
     * {@inheritDoc}
     */
    @Override
    public boolean isUsernameAvailable(String username) {
        return userAccountRepository.findByUsername(username).orElse(null) == null;
    }

    /**
     * This method send user in tutorial management service to persist some infos in its own DB.
     * @param accessToken token to propagate
     * @param accountDto user to send
     */
    private void persistInTutorialManagementDb(String accessToken, UserAccountDto accountDto) {
        try {
            httpProxy.saveUserInTutorialManagementDb(accessToken, accountDto);
        }catch (Exception e){
            log.error("Error during user account persist process in tutorial service DB : " + e.getMessage());
            keycloakUtils.deleteUser(accountDto.getExternalId());
            throw new FunctionalException("Error during user account persist process in tutorial service DB : " + e.getMessage());
        }
    }

    /**
     * This method send user in commentary management service to persist some infos in its own DB.
     * @param accessToken token to propagate
     * @param accountDto user to send
     */
    private void persistInCommentaryManagementDb(String accessToken, UserAccountDto accountDto) {
        try {
            httpProxy.saveUserInCommentaryManagementDb(accessToken, accountDto);
        }catch (Exception e){
            log.error("Error during user account persist process in commentary service DB : " + e.getMessage());
            keycloakUtils.deleteUser(accountDto.getExternalId());
            httpProxy.removeUserInTutorialManagementDb(accessToken,accountDto.getExternalId());
            throw new FunctionalException("Error during user account persist process in commentary service DB : " + e.getMessage());
        }
    }

    /**
     * This method persist user in DB
     * @param accountDto user to persist
     * @param keycloakId id generate by keycloak auth server. we use it as external id for user
     * @return user if insertion in DB is successful
     */
    private UserAccountDto persistInAccountManagementDb(UserAccountDto accountDto, String keycloakId) {
        accountDto.setExternalId(keycloakId);
        accountDto.setRegistrationAccountDate(LocalDateTime.now());
        try {
            return userAccountMapper.fromEntityToDto(
                    userAccountRepository.save(userAccountMapper.fromDtoToEntity(accountDto))
            );
        } catch (Exception e){
            log.error("Error during user account persist process in DB : " + e.getMessage());
            keycloakUtils.deleteUser(keycloakId);
            throw new FunctionalException("Error during user account persist process in DB : " + e.getMessage());
        }
    }

    /**
     * This method call keycloakUtil service to persist user in Keycloak auth server
     * @param accountDto user to persist
     * @return id generate by Keycloak auth server
     */
    private String persistInKeycloakAuthServer(UserAccountDto accountDto) {
        try{
            return keycloakUtils.createUser(accountDto);
        }catch (WebApplicationException e){
            log.error("Error during keycloak user creation : " + e.getMessage());
            throw new FunctionalException("Error during keycloak user creation : " + e.getMessage());
        }
    }

}
