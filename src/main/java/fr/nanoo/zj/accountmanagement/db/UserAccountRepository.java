package fr.nanoo.zj.accountmanagement.db;

import fr.nanoo.zj.accountmanagement.model.entities.UserAccount;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * @author nanoo - created : 26/05/2020 - 14:45
 */
@Repository
public interface UserAccountRepository extends JpaRepository<UserAccount, Long> {

    Optional<UserAccount> findByExternalId(String externalId);
    Optional<UserAccount> findByEmail(String mail);
    Optional<UserAccount> findByUsername(String username);

    @Modifying
    @Query(value="DELETE FROM UserAccount user WHERE user.externalId = :externalId")
    void deleteByExternalId(String externalId);
}
