package fr.nanoo.zj.accountmanagement.model.entities;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * @author nanoo - created : 26/05/2020 - 13:12
 */
@Data
@Entity
@Table(name = "userAccount")
public class UserAccount implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id_user_account")
    private Long userAccountId;

    @Column(name = "external_id",unique = true, nullable = false)  // check if keycloak user id always same length
    private String externalId;

    @Column(name = "first_name", length = 32, nullable = false)
    private String firstName;

    @Column(name = "last_name", length = 32, nullable = false)
    private String lastName;

    @Column(name = "username", length = 32)
    private String username;

    @Column(name = "email", unique = true, nullable = false)
    private String email;

    @Column(name = "biography", length = 400)
    private String biography;

    @Column(name = "registration_account", nullable = false)
    private LocalDateTime registrationAccountDate;

    @Column(name = "update_account")
    private LocalDateTime updateAccountDate;

}
