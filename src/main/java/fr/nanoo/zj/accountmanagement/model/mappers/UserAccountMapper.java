package fr.nanoo.zj.accountmanagement.model.mappers;

import fr.nanoo.zj.accountmanagement.model.dtos.UserAccountDto;
import fr.nanoo.zj.accountmanagement.model.entities.UserAccount;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

import java.util.List;

/**
 * @author nanoo - created : 26/05/2020 - 14:36
 */
@Mapper(componentModel = "spring",
        unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface UserAccountMapper {

    UserAccount fromDtoToEntity (UserAccountDto accountDto);
    UserAccountDto fromEntityToDto (UserAccount account);
    List<UserAccount> fromDtoListToEntityList (List<UserAccountDto> accountDtos);
    List<UserAccountDto> fromEntityListToDtoList (List<UserAccount> accounts);

}
