package fr.nanoo.zj.accountmanagement.model.dtos;

import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * @author nanoo - created : 26/05/2020 - 14:24
 */
@Data
public class UserAccountDto implements Serializable {

    private static final long serialVersionUID = 1L;

    private String externalId;
    private String firstName;
    private String lastName;
    private String password;
    private String username;
    private String email;
    private String biography;
    private LocalDateTime registrationAccountDate;

}
