package fr.nanoo.zj.accountmanagement.application.controller;

import fr.nanoo.zj.accountmanagement.application.errors.FunctionalException;
import fr.nanoo.zj.accountmanagement.model.dtos.UserAccountDto;
import fr.nanoo.zj.accountmanagement.security.SecurityUtils;
import fr.nanoo.zj.accountmanagement.service.contract.UserAccountService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.servlet.http.HttpServletRequest;

/**
 * @author nanoo - created : 28/06/2020 - 16:11
 */
@PreAuthorize("permitAll()")
@RestController
@Slf4j
public class UserCreationController {

    private final UserAccountService userAccountService;

    @Autowired
    public UserCreationController(UserAccountService userAccountService) {
        this.userAccountService = userAccountService;
    }


    @PutMapping("/account/save")
    public UserAccountDto saveAccount (@RequestBody UserAccountDto accountDto, HttpServletRequest request){
        log.debug("save account method entry");
        try {
            String accessToken = SecurityUtils.getToken(request);
            return userAccountService.createAccount(accessToken, accountDto);
        }catch (FunctionalException e){
            throw new ResponseStatusException(
                    HttpStatus.BAD_REQUEST, "Error during user creation",e
            );
        }
    }

    @GetMapping("/account/check/mail")
    public boolean isMailAvailable (@RequestParam String mail){
        log.debug("check mail account method entry");
        return userAccountService.isMailAvailable(mail);
    }

    @GetMapping("/account/check/username")
    public boolean isUsernameAvailable (@RequestParam String username){
        log.debug("check username account method entry");
        return userAccountService.isUsernameAvailable(username);
    }

}
