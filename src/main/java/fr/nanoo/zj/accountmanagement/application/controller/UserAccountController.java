package fr.nanoo.zj.accountmanagement.application.controller;

import fr.nanoo.zj.accountmanagement.application.errors.FunctionalException;
import fr.nanoo.zj.accountmanagement.model.dtos.UserAccountDto;
import fr.nanoo.zj.accountmanagement.security.SecurityUtils;
import fr.nanoo.zj.accountmanagement.service.contract.UserAccountService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * @author nanoo - created : 26/05/2020 - 15:49
 */
@PreAuthorize("isAuthenticated()")
@RestController
@Slf4j
public class UserAccountController {

    private final UserAccountService userAccountService;

    @Autowired
    public UserAccountController(UserAccountService userAccountService) {
        this.userAccountService = userAccountService;
    }

    @PostAuthorize("returnObject.username.toLowerCase() == authentication.principal")  // need 'toLowerCase()' because Keycloak save username without case sensitive
    @GetMapping("/account/{id}")
    public UserAccountDto getAccountByExternalId(@PathVariable("id") String externalId){
        log.debug("get account by external id entry");
        try {
            return userAccountService.getAccountByExternalId(externalId);
        }catch (Exception e){
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND, "user not found", e
            );
        }
    }

    @PreAuthorize("hasRole('admin') or #id == authentication.credentials")
    @DeleteMapping("/account/remove/{id}")
    public void removeAccount(@PathVariable("id") String id, HttpServletRequest request){
        log.debug("delete account by external id entry");
        try {
            String accessToken = SecurityUtils.getToken(request);
            userAccountService.deleteUserAccount(accessToken, id);
        }catch (FunctionalException e) {
            throw new ResponseStatusException(
                    HttpStatus.BAD_REQUEST, "Error during user deletion", e
            );
        }
    }

    @PreAuthorize("hasRole('admin')")
    @GetMapping("/accounts")
    public List<UserAccountDto> getAccountList (){
        log.debug("get account list entry");
        return userAccountService.getUserAccountList();
    }

}
