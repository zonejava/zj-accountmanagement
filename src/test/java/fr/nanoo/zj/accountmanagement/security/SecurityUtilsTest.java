package fr.nanoo.zj.accountmanagement.security;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
class SecurityUtilsTest {

    @Test
    void whenGetToken_GivenHttpRequestWithTokenInHeader_ThenReturnTokenAsString() {
        MockHttpServletRequest request = new MockHttpServletRequest();
        request.addHeader("Authorization","Bearer eyxxxxx");

        String result = SecurityUtils.getToken(request);

        assertThat(result).isEqualTo("eyxxxxx");
    }
}