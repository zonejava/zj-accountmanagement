package fr.nanoo.zj.accountmanagement.service.impl;

import fr.nanoo.zj.accountmanagement.application.errors.FunctionalException;
import fr.nanoo.zj.accountmanagement.db.UserAccountRepository;
import fr.nanoo.zj.accountmanagement.model.dtos.UserAccountDto;
import fr.nanoo.zj.accountmanagement.model.entities.UserAccount;
import fr.nanoo.zj.accountmanagement.model.mappers.UserAccountMapper;
import fr.nanoo.zj.accountmanagement.model.mappers.UserAccountMapperImpl;
import fr.nanoo.zj.accountmanagement.service.contract.UserAccountService;
import fr.nanoo.zj.accountmanagement.utils.feign.HttpProxy;
import fr.nanoo.zj.accountmanagement.utils.keycloak.KeycloakUtils;
import org.hibernate.HibernateException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;

import javax.ws.rs.WebApplicationException;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;

@SpringJUnitConfig(value={UserAccountServiceImpl.class, UserAccountMapperImpl.class})
class UserAccountServiceImplTest {

    @Autowired
    private UserAccountService classUnderTest;
    @Autowired
    private UserAccountMapper accountMapper;

    @MockBean
    private UserAccountRepository userAccountRepository;
    @MockBean
    private KeycloakUtils keycloakUtils;
    @MockBean
    private HttpProxy httpProxy;

    private UserAccountDto userAccountDto;
    private String accessToken;

    @BeforeEach
    void setUp() {
        userAccountDto = new UserAccountDto();
        accessToken = "";
    }

    @Test
    void whenPersistInKeycloakAuthServer_GivenUserAccount_ThenThrowFunctionnalException() {
        given(keycloakUtils.createUser(any(UserAccountDto.class))).willThrow(new WebApplicationException("test"){});

        FunctionalException exception =
                assertThrows(FunctionalException.class, () -> classUnderTest.createAccount(accessToken, userAccountDto));

        assertThat(exception.getMessage()).isEqualTo("Error during keycloak user creation : test");
    }

    @Test
    void whenPersistInAccountManagementDb_GivenUserAccount_ThenThrowFunctionnalException() {
        given(userAccountRepository.save(any(UserAccount.class))).willThrow(new HibernateException("test"){});

        FunctionalException exception =
                assertThrows(FunctionalException.class, () -> classUnderTest.createAccount(accessToken, userAccountDto));

        assertThat(exception.getMessage()).isEqualTo("Error during user account persist process in DB : test");
    }

}