package fr.nanoo.zj.accountmanagement.service.impl;

import fr.nanoo.zj.accountmanagement.db.UserAccountRepository;
import fr.nanoo.zj.accountmanagement.model.dtos.UserAccountDto;
import fr.nanoo.zj.accountmanagement.model.entities.UserAccount;
import fr.nanoo.zj.accountmanagement.service.contract.UserAccountService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
@Transactional
class UserAccountServiceImplIT {

    @Autowired
    private UserAccountService classUnderTest;

    @Autowired
    private UserAccountRepository userAccountRepository;

    @BeforeEach
    void init() {
        userAccountRepository.deleteAll();

        UserAccount account1 = new UserAccount();
        account1.setExternalId("xxx1");
        account1.setFirstName("john");
        account1.setLastName("doe");
        account1.setUsername("johnD");
        account1.setEmail("john@mail.fr");
        account1.setRegistrationAccountDate(LocalDateTime.now());
        UserAccount account2 = new UserAccount();
        account2.setExternalId("xxx2");
        account2.setFirstName("marc");
        account2.setLastName("ini");
        account2.setUsername("marcI");
        account2.setEmail("marc@mail.fr");
        account2.setRegistrationAccountDate(LocalDateTime.now());

        userAccountRepository.saveAll(Arrays.asList(account1,account2));
    }

    @Test
    void getUserAccountList() {
        List<UserAccountDto> result = classUnderTest.getUserAccountList();

        assertThat(result.size()).isEqualTo(2);
    }

    @Test
    void getAccountByExternalId() {
        UserAccountDto result = classUnderTest.getAccountByExternalId("xxx1");

        assertThat(result.getUsername()).isEqualTo("johnD");
    }

    @Test
    void isMailAvailable() {
        boolean result = classUnderTest.isMailAvailable("john@mail.fr");

        assertThat(result).isFalse();

        boolean result2 = classUnderTest.isMailAvailable("jean@mail.fr");

        assertThat(result2).isTrue();
    }

    @Test
    void isUsernameAvailable() {
        boolean result = classUnderTest.isUsernameAvailable("johnD")    ;

        assertThat(result).isFalse();

        boolean result2 = classUnderTest.isUsernameAvailable("jeanC");

        assertThat(result2).isTrue();
    }
}